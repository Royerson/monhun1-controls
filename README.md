# monhun1 controls

AutoHotkey script to change Monster Hunter 1 and G's controls to use buttons for attacks instead of the right stick.

Only works for XInput controllers. The script can potentially get out of sync with whether your weapon is sheathed or not. Ideally it would read some memory value to detect this, but I don't know how to do that.

**Important:** Put the PCSX2_keys.ini in PCSX2's inis folder, or the script will cause fastforwarding and savestate saving/loading which can disconnect you from the server.


### Instructions
Put PCSX2_keys.ini in the inis folder, load one of the .lily files in LilyPad, and run the script.

The script can also start PCSX2 for you and close itself when PCSX2 closes. To do so, change pcsx2cmd to a command that fits your system and uncomment it and the lines following it.

## Controls
Controls in bold are non-default.

| keyboard | action |
| ------ | ------ |
| ` (grave/tilde) | **opens chat for english typing** |

| XInput | action |
| ------ | ------ |
| L-stick | movement |
| R-stick | **camera / navigate menus** |
| D-pad | camera / navigate menus |
| A (Cross) | cancel (menus) / roll |
| B (Circle) | confirm (menus) / interact (sheathed) / **medium attack (unsheathed)** |
| X (Square) | use item / sheath |
| Y (Triangle) | **light attack** |
| Y+B (△+O) | **heavy attack** |
| L1 | reset camera |
| R1 | shield |
| L2 | item wheel |
| R2 | run |


### basic.lily

| XInput | action |
| ------ | ------ |
| Start | menu |
| Select | map toggle |
| L3 | **chat / elder advice** |
| R3 | kick |

### pro.lily
| XInput | action |
| ------ | ------ |
| Start | **kick** |
| Select | **menu** |
| L3 | **map toggle** |
| R3 | **chat / elder advice** |
