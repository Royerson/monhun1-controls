#Include <XInput>
XInput_Init()

#SingleInstance ignore
SetStoreCapsLockMode, off
SetKeyDelay, 40, 40


pcsx2exe := "ahk_exe pcsx2.exe"

TO := 0
sheathed := true

SetTimer, joyInput, 8


;pcsx2cmd := "C:\games\pcsx2-v1.7.0-dev-501-g7a2c94f6e-windows-x86\pcsx2.exe ""C:\games\pcsx2-v1.7.0-dev-501-g7a2c94f6e-windows-x86\iso\Monster Hunter (Japan) [en v6].iso"" --nogui"
;if(!WinExist(pcsx2exe))
;	run % pcsx2cmd
;sleep 10000
;SetTimer, mainLoop, 1000

return

mainLoop:
	if(!WinExist(pcsx2exe)) {
		ExitApp
	}
return

joyInput:
	if( !WinActive(pcsx2exe, "panel") ) {
		VarSetCapacity(lastState, 16)
		suspend, on
		return
	} else {
		suspend, off
	}
	
	state := XInput_GetState(0)
	
	if ( !state.bLeftTrigger ) { ;item wheel cancels all these effects
		if ( isJoyJustPressed(XINPUT_GAMEPAD_X) ) { ;sheath
			sheathed := true
		}
		if ( state.bRightTrigger AND !lastState.bRightTrigger ) { ;run
			sheathed := true
		}
		if ( isJoyPressed(XINPUT_GAMEPAD_RIGHT_SHOULDER) ) { ;shield
			sheathed := false
		}
	}
	
	if ( isJoyJustPressed(XINPUT_GAMEPAD_B) ) {
		TOinput(XINPUT_GAMEPAD_B)
	}
	if ( isJoyJustPressed(XINPUT_GAMEPAD_Y) ) {
		sheathed := false
		TOinput(XINPUT_GAMEPAD_Y)
	}
	
	lastState := state
return

TOoutput:
	if (TO == XINPUT_GAMEPAD_B + XINPUT_GAMEPAD_Y) {
		send {F4} ;heavy
	}
	if (TO == XINPUT_GAMEPAD_Y) {
		send {F2} ;light
	}
	if (TO == XINPUT_GAMEPAD_B) {
		if (sheathed OR state.bLeftTrigger) {
			send {F1} ;circle
		} else {
			send {F3} ;medium
		}
	}
	TO := 0
return

TOinput(bit)
{
	global TO
	if (!TO) {
		SetTimer, TOoutput, -16
	}
	
	TO |= bit
}

isJoyJustPressed(bit)
{
	global state
	global lastState
	return (state.wButtons & bit) AND !(lastState.wButtons & bit)
}
isJoyJustReleased(bit)
{
	global state
	global lastState
	return !(state.wButtons & bit) AND (lastState.wButtons & bit)
}
isJoyPressed(bit) {
	global state
	return bit & state.wButtons
}

;opens chat and sets it to english, lowercase, and regular width
`::                 ;chat is opened by ` and AHK can't block input from CLR_USB
	sleep 200       ;chat takes a bit to open
	tmpCaps := GetKeyState("CapsLock", "T")
	send {CapsLock} ;toggles to romanji
	SetCapsLockState % tmpCaps
	send,``         ; ` toggles to regular width
	send {F12}      ;R2, first press gives controller chat control
	send {F12}      ;R2, second press switches to lowercase
return


